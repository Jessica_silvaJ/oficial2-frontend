import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Menu from './Paginas/Menu';
import Rodape from './Paginas/Rodape';
import Inicio from './Paginas/Inicio';
import Formulario from './Paginas/Formulario';
import Pagamento from './Paginas/Pagamento';
import Lista from './Paginas/ListadeProduto';
import Mensagem from './Paginas/Mensagem';



function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Menu />
        <Switch>
        
          <Route exact path="/" component={Inicio} />
          <Route path="/ListadeProduto" component={Lista} />
          <Route path="/formulario" component={Formulario} />
          <Route path="/Pagamento" component={Pagamento} />
          <Route path="/Msg" component={Mensagem} />
           
        </Switch>
       <Rodape />
      </BrowserRouter>

    </div>
  );
}

export default App;
