import React from 'react';
import { Redirect } from 'react-router';
import Buys from '../Img/buys.jpg';

class Succ extends React.Component {
    constructor(props: any) {
        super(props);
    }
    state = {

        redirect: false,

    }


    volta = () => {
        this.setState({
            redirect: true
        })
    }


    render() {
        if (this.state.redirect) {
            return <Redirect to="/" />

        }
        else {
            return (


                <div className=" Mensag">
                    <div>
                    <img src={Buys} width='100%' height='400px' alt='logo' />
                    </div>

                    <div className="title-1">
                        <hr/>
                        <h1>COMPRA REALIZADA COM SUCESSO</h1>
                        <hr/>
                    </div>

                    <div className='msg'>

                        <h2>  COMPRA REALIZADA </h2>
                        <p>Obrigado por comprar no nossos site , seu dados foi salvo com segurança<br/>
                        aperte no botão para volta a pagina inicial</p>
                        <input  className=''type="submit" value="VOLTA PAGINA INICIAL" onClick={() => this.volta()} />

                    </div>

                </div>

            );
        }
    }
};

export default Succ;