import React from 'react';
import {Link} from 'react-router-dom';
import Cart from '../Img/cart.png';
import logo from '../Img/Logo.png';
const Menu : React.FC = () => {
    return(
        <div>
             <nav className='Menu'> 
              <ul>
              <img src={logo} width='15px' height='15px'/>&nbsp;&nbsp;<p>ELECTRONIC STORE</p>

              &nbsp;&nbsp;&nbsp;&nbsp;
             <li><Link to='/'>INICIO</Link></li>
                   <li><Link to='/ListadeProduto'>LISTA DE PRODUTOS</Link></li>
                   <li><Link to='/formulario'>CADASTRAR PEDIDO</Link></li>
                   <li><Link to='/Pagamento'>PAGAMENTO</Link></li>
                   <li><Link to='/Msg'>MENSAGEM</Link></li>
                   <img src={Cart} width='15px' height='15px'/>            

              </ul>
         </nav>
                   
                   
        </div>
         
    );
};

export default Menu;