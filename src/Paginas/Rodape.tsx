import React from 'react';
import { Link } from 'react-router-dom';

const Rodape: React.FC = () => {
  return (

    <div>
      <footer className="site-footer">
        <div className="zigzag-bottom"></div>
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-md-6">
              <h6>Sobre</h6>
              <p className="text-justify">Electronic Store é uma loja brasileira de e-commerce<br />
              com diversos tipos de tecnologia que atendar as suas necessidades, <br />
              estamos com varios projeto para 2020, por conta dessa pandemia <br />
              teremos um atraso!!</p>
            </div>

            <div className="col-xs-6 col-md-3">
              <h6>Categorias</h6>
              <ul className="footer-links">
                <li><a>Celulares</a></li>
                <li><a>Notebook</a></li>
                <li><a>Televisão</a></li>

              </ul>
            </div>

            <div className="col-xs-6 col-md-3">
              <h6> Links</h6>
              <ul className="footer-links">
                <li><Link to='/'>INICIO</Link></li>
                <li><Link to='/ListadeProduto'>LISTA DE PRODUTOS</Link></li>
                <li><Link to='/formulario'>DADOS DO CLIENTE</Link></li>
                <li><Link to='/Pagamento'>PAGAMENTO</Link></li>
                <li><Link to='/Msg'>MENSAGEM</Link></li>
              </ul>
            </div>
          </div>
          <hr>
          </hr>
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-sm-6 col-xs-12">
                <p className="copyright-text"> @2020, Direitos autorais&nbsp;
         <a href="#">Electronic Store</a>
                </p>
              </div>
            </div>
               
               
          </div>
            

        </div>
      </footer>

    </div>
  );
};

export default Rodape;