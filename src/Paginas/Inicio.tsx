import React from 'react';
import Return from '../Img/return.png';
import Truck from '../Img/truck.png';
import Lock from '../Img/lock.png';
import Produts from '../Img/produts.png';
import Img from '../Img/Img.jpg';
import Cell from '../Img/Cell.jpg';
import MG8 from '../Img/Mg8.jpg';
import Lnovo from '../Img/Lnovo.png';
import TV from '../Img/Tv.png';
import Phone from '../Img/Phone.png';
import Console from '../Img/console.jpg';
import Memoria from '../Img/Memoria.jpg';
import FIFA from '../Img/Fifa.jpg';
import { Redirect } from 'react-router';


class Inicio extends React.Component {

    constructor(props: any) {
        super(props);


    }
    state = {

        redirect: false,

    }

    chamaForm = () => {
        this.setState({
            redirect: true
        })
    }



    render() {

        if (this.state.redirect) {
            return <Redirect to="/formulario" />

        }
        else {

            return (

                <div className='Inicio'>

                    <div className="logo">
                        <img src={Img} width='100%' height='500px' alt='logo' />

                    </div>

                    <div className="product">

                        <div className='section-title'>
                            <hr/>
                        <h2>Produtos Mais Recentes</h2>
                        <hr/>
                        </div>

                        <div className='Celular'>
                            <p></p>
                            <img src={Cell} width='158px' height='158px' alt='logo' />
                            <p>Celular Smartphone Galaxy A30s<br /> 64GB 6,4" Samsung - Branco</p>
                            <p>por: R$ 1.329,05</p>
                            <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />
                        </div>

                        <div className='Lnovo'>

                            <img src={Lnovo} width='158px' height='168px' alt='logo' />
                            <p>Notebook Lenovo Ideapad 330<br />  Intel Core I5, 8GB, 1T</p>
                            <p>por: R$ R$ 2.699,00</p>
                            <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                        </div>

                        <div className='TV'>

                            <img src={TV} width='158px' height='168px' alt='logo' />
                            <p>Smart TV LED 32" Samsung<br /> Digital 2 HDMI 1 USB</p>
                            <p>por: R$  1.199,00</p>
                            <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                        </div>

                        <div className='MG8'>
                            <img src={MG8} width='158px' height='168px' alt='logo' />
                            <p>Smartphone Moto G8 Play<br /> 32GB 6,2" - Vermelho Magenta</p>
                            <p>por: R$ 1.098,00</p>
                            <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                        </div>


                    </div>

                    <div className="promo-area">

                        <div className="zigzag-bottom"></div>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-3 col-sm-6">
                                    <div className="single-promo">
                                        <img src={Return} width='58px' height='58px' alt='logo' />
                                        <p>Returno de 30 dias</p>
                                    </div>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <div className="single-promo">
                                        <img src={Truck} width='58px' height='58px' alt='logo' />

                                        <p>Envio Grátis</p>
                                    </div>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <div className="single-promo">
                                        <img src={Lock} width='58px' height='58px' alt='logo' />
                                        <p>Pagamento Seguro</p>
                                    </div>
                                </div>
                                <div className="col-md-3 col-sm-6">

                                    <div className="single-promo">
                                        <img src={Produts} width='58px' height='58px' alt='logo' />
                                        <p>Novos Produtos</p>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>
                    
                    <div className='section-title'>
                        <hr />
                        <h2>Mercadorias que estaram disponivel em  BREVE !!!</h2>
                        <hr />
                    </div>
                    
                    <div className='Celular'>
                        <p></p>
                        <img src={Phone} width='158px' height='158px' alt='logo' />
                        <p>iPhone 11 Pro Apple com 256GB<br /> Resistente à Água </p>
                        <p>por: R$ 6.479,10</p>
                        <input type="submit" value=" INDISPONIVEL" />
                    </div>

                    <div className='Lnovo'>

                        <img src={Console} width='158px' height='168px' alt='logo' />
                        <p>Console Microsoft Xbox One S<br /> TB All Digital Edition V2</p>
                        <p>por: R$ 899,00</p>
                        <input type="submit" value="INDISPONIVEL" />
                    </div>

                    <div className='Celular'>

                        <img src={Memoria} width='158px' height='168px' alt='logo' />
                        <p>Cartão de Memoria 64gb <br /> Ultra SDSQUNS Sandisk</p>
                        <p>por: R$ 77,31</p>
                        <input type="submit" value="INDISPONIVEL" />

                    </div>

                    <div className='MG8'>
                        <img src={FIFA} width='158px' height='168px' alt='logo' />
                        <p>Game - FIFA 20 <br /> Xbox One</p>
                        <p>por: R$ 185,35</p>
                        <input  type="submit" value="INDISPONIVEL" />

                    </div>

                </div>
            );
        }
    }
};

export default Inicio;