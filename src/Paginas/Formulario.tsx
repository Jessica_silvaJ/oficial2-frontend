import React from 'react';
import { Redirect } from 'react-router';
import Cad from '../Img/cadastro.jpg';

class form extends React.Component {
    constructor(props: any) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.Nome = this.Nome.bind(this);
        this.Email = this.Email.bind(this);
        this.Cpf = this.Cpf.bind(this);
        this.Estado = this.Estado.bind(this);

    }
    state = {
        nome: '',
        cpf: '',
        email: ' ',
        estado: '',
        redirect: false,


        address: {

            cep: "",
            city: "",
            neighborhood: "",
            state: "",
            street: ""
        },
        cepInput: ""

    }

    onInputChange(event: any) {
        this.setState({
            cepInput: event.target.value
        })
    }

    Nome = (event: any) => {

        this.setState({ nome: event.target.value });
    }


    Email = (event: any) => {
        this.setState({ email: event.target.value });
    }

    Cpf = (event: any) => {
        this.setState({ cpf: event.target.value });
    }

    Estado = (event: any) => {
        this.setState({ estado: event.target.value });
    }


    handleSubmit(event: any) {
        fetch(`http://localhost:3001/cep?value=${this.state.cepInput}`)
            .then(
                res => res.json()
            ).then(addressFromApi => {
                this.setState({
                    address: addressFromApi
                })
                      
    
            })
            
             

            .catch (error => {
            console.log(error);
        })
        ;

        event.preventDefault();
    }




    //   redirect
    Form = () => {
        this.setState({
            redirect: true
        })

    }





    render() {


        if (this.state.redirect) {

            return < Redirect to="/pagamento" />

        }
        else {

            return (
                <div>
                    <div>
                    <img src={Cad} width='100%' height='400px' alt='logo' />

                    </div>

                    <div className="title-1">
                        <hr/>
                        <h1>PREENCHA COM OS SEUS DADOS</h1>
                        <hr/>
                    </div>


                    <form className='Corpo-F'>

                        <div className='Nome'>
                            <h4> Nome: </h4>
                            <input type='text' className='form-control' name='nome' placeholder=' Digite o seu completo!' required value={this.state.nome} onChange={this.Nome} />

                        </div>

                        <div className='E-mail'>
                            <label>
                                <h4>E-mail: </h4>
                                <input type='email' className='form-control' name='email' placeholder='Por exemplo mauro@email.com' required value={this.state.email} onChange={this.Email} />
                            </label>
                        </div>


                        <div className='CPF'>
                            <label>

                                Cpf:  <input type='text' className='form-control' name='CPF' placeholder='Por exemplo: 000.000.000-00' required value={this.state.cpf} onChange={this.Cpf} />
                            </label>
                        </div>



                        <div className='Cep'>
                            <label>Cep: </label>
                            <input onChange={event => this.onInputChange(event)}  onSubmit={this.handleSubmit}  />

                        </div>

                        <div className='EST'>
                            <label> <h4>Estado: </h4></label>
            &nbsp;<select value={this.state.estado} onChange={this.Estado} name="Estados" required>
                                <option value="">Selecione</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AM">Amazonas</option>
                                <option value="AP">Amapá</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espírito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RO">Rondônia</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SE">Sergipe</option>
                                <option value="SP">São Paulo</option>
                                <option value="TO">Tocantins</option>
                            </select>
                        </div>

                        <div className='Cidade'>
                            <label>Cidade: </label>
                            <input onChange={event => this.onInputChange(event)} />
                        </div>

                        <div className='Bairro'>
                            <label>Bairro: </label>
                            <input onChange={event => this.onInputChange(event)} />
                        </div>

                        <div className='Rua'>
                            <label>Rua: </label>
                            <input onChange={event => this.onInputChange(event)} />
                        </div>


                        <div className='button'>
                            <input type="submit" value="PROXIMO" onClick={() => this.Form()}/>
                        </div>
                    </form>

                </div >
            )

        }

    };
}

export default form;
