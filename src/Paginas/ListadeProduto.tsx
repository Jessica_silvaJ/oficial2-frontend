import React from 'react';
import { Redirect } from 'react-router';
import Prod from '../Img/prod.jpg';

import Cell from '../Img/Cell.jpg';
import MG8 from '../Img/Mg8.jpg';
import Xiami from '../Img/xiami.jpg';
import LGp from '../Img/LG-1.jpg';
import GE from '../Img/G-E.jpg';
import M from "../Img/51.jpg";
import LGS from '../Img/LGS.jpg';
import Nokia from '../Img/Nokia.jpg';


import Lnovo from '../Img/Lnovo.png';
import POS from '../Img/POS.png';
import GAMER from '../Img/Gamer.jpg';
import DELL from '../Img/Dell.jpg';
import note from '../Img/note.jpg';
import noteS from '../Img/noteS.jpg';
import noteP from '../Img/noteP.jpg';
import noteV from '../Img/noteV.jpg';

import TV from '../Img/Tv.png';
import LG from '../Img/LG.jpg';
import TOSH from '../Img/TOSH.png';
import PHILCO from '../Img/PHILCO.jpg';

class Inicio extends React.Component {

    constructor(props: any) {
        super(props);


    }
    state = {

        redirect: false,

    }

    chamaForm = () => {
        this.setState({
            redirect: true
        })
    }



    render() {

        if (this.state.redirect) {
            return <Redirect to="/formulario" />

        }
        else {

            return (

                <div className='ListaProdutos'>

                    <div className="">
                        <img src={Prod} width='100%' height='500px' alt='logo' />
                    </div>


                    <div className='title-1' >
                        <hr />
                        <h2>CELULARES</h2>
                        <hr />
                    </div>

                    <div className='Celular'>
                        <p></p>
                        <img src={Cell} width='158px' height='158px' alt='logo' />
                        <p>Celular Smartphone Galaxy A30s<br /> 64GB 6,4" Samsung - Branco</p>
                        <p>por: R$ 1.329,05</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />
                    </div>

                    <div className='MG8'>
                        <img src={MG8} width='158px' height='168px' alt='logo' />
                        <p>Smartphone Moto G8 Play<br /> 32GB 6,2" - Vermelho Magenta</p>
                        <p>por: R$ 1.098,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    <div className='LG'>

                        <img src={LGp} width='158px' height='170px' alt='logo' />
                        <p>Smartphone K12 <br /> </p>
                        <p>por: R$ 919,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>


                    <div className='Xiami'>

                        <img src={Xiami} width='158px' height='188px' alt='logo' />
                        <p>Smartphone Xiaomi Redmi Note 8</p>
                        <p>por: R$ 2.540,90</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    {/* Segunda parte */}

                    <div className='Celular'>
                        <p></p>
                        <img src={GE} width='158px' height='158px' alt='logo' />
                        <p>Smartphone Motorola Moto E6 <br /> Plus Azul Netuno 32GB</p>
                        <p>por:R$ 949,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />
                    </div>

                    <div className='MG8'>
                        <img src={M} width='158px' height='168px' alt='logo' />
                        <p>Smartphone Samsung Galaxy A51 <br /> Tela Infinita de 6.5</p>
                        <p>por: R$ 1.899,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    <div className='LG'>

                        <img src={LGS} width='158px' height='170px' alt='logo' />
                        <p>Smartphone LG K50S Azul<br /> Inteligência Artificial</p>
                        <p>por: R$ 999,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    <div className='Xiami'>

                        <img src={Nokia} width='178px' height='190px' alt='logo' />
                        <p>Smartphone Nokia 2.3 32GB</p> 
                        <p>por: R$ 899,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>





                    <div className='title-2'>

                        <hr />
                        <h2>NOTEBOOK</h2>
                         <hr />
                    </div>
                   

                    <div className='Lnovo'>

                        <img src={Lnovo} width='158px' height='168px' alt='logo' />
                        <p>Notebook Lenovo Ideapad 330<br />  Intel Core I5, 8GB, 1T</p>
                        <p>por: R$ R$ 2.699,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    <div className='POS'>

                        <img src={POS} width='158px' height='168px' alt='logo' />
                        <p>Notebook Motion Q232A<br />  Intel Atom Quad Core 2GB </p>
                        <p>por: R$ 999,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    <div className='Gamer'>

                        <img src={GAMER} width='158px' height='188px' alt='logo' />
                        <p>Notebook Gamer Lenovo<br />  </p>
                        <p>por: 1.157,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    <div className='DELL'>

                        <img src={DELL} width='158px' height='188px' alt='logo' />
                        <p>Notebook Gamer Lenovo<br />   </p>
                        <p>por: 1.157,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    {/* Segunda parte */}

                    <div className='DELL'>

                        <img src={note} width='158px' height='188px' alt='logo' />
                        <p>Notebook Samsung Dual Core<br /> </p>
                        <p>por: R$ 2.249,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>
                    <div className='DELL'>

                        <img src={noteS} width='158px' height='168px' alt='logo' />
                        <p>Notebook Samsung Dual<br /></p>
                        <p>por: R$ 2.199,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>
                    <div className='DELL'>

                        <img src={noteP} width='158px' height='168px' alt='logo' />
                        <p>Notebook Positivo Quad Core<br /> </p>
                        <p>por: R$ 1.157,00 </p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>
                    <div className='DELL'>

                        <img src={noteV} width='158px' height='168px' alt='logo' />
                        <p>Notebook Vaio Core<br /></p>
                        <p>por: R$ 4.299,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>


                    <div className='title-3'>
                        <hr />
                        <h2>
                            TELEVISÃO
                           </h2>
                        <hr />
                    </div>


                    <div className='TV'>

                        <img src={TV} width='158px' height='168px' alt='logo' />
                        <p>Smart TV LED 32" Samsung<br /> Digital 2 HDMI 1 USB</p>
                        <p>por: R$ 1.199,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    <div className='LG'>

                        <img src={LG} width='158px' height='168px' alt='logo' />
                        <p>Smart TV Led 32<br />  LG HD Thinq AI Conversor</p>
                        <p>por: R$ 1.249,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    <div className='TOSH'>

                        <img src={TOSH} width='158px' height='168px' alt='logo' />
                        <p>Smart TV LED 32<br /> Toshiba 32L2800 HD 3 HDMI 2 USB</p>
                        <p>por: R$ 1.220,00</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                    <div className='PHILCO'>

                        <img src={PHILCO} width='158px' height='168px' alt='logo' />
                        <p>Smart TV LED 32<br />  Philco PTV32G52S HD e Áudio</p>
                        <p>por R$ 1.049,90</p>
                        <input type="submit" value="COMPRAR PRODUTO" onClick={() => this.chamaForm()} />

                    </div>

                </div>
            );
        }
    }
};

export default Inicio;