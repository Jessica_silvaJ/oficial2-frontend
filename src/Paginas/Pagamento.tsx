import React from 'react';
import { Redirect } from 'react-router';
import Pagment from '../Img/pp.jpg';

class form extends React.Component {
  constructor(props: any) {
    super(props);

    this.Pagamento = this.Pagamento.bind(this);
    this.Cnome = this.Cnome.bind(this);
    this.NumeroC = this.NumeroC.bind(this);
    this.Data = this.Data.bind(this);
    this.CVV = this.CVV.bind(this);


  }
  state = {

    Pagamento: '',
    NumeroCartao: '',
    Cnome: '',
    Data: '',
    cvv: '',
    redirect: false,

  }





  Pagamento = (event: any) => {
    this.setState({ Pagamento: event.target.value });

  }

  Cnome = (event: any) => {

    this.setState({ Cnome: event.target.value });
  }

  NumeroC = (event: any) => {

    this.setState({ NumeroCartao: event.target.value });
  }

  Data = (event: any) => {

    this.setState({ Data: event.target.value });
  }

  CVV = (event: any) => {

    this.setState({ cvv: event.target.value });
  }


  // redirect

  chamaFuction = () => {
    this.setState({
      redirect: true
    })
  }



  render() {

    if (this.state.redirect) {
      return <Redirect to="/Msg" />

    }
    else {

      return (
        <div>
          <div className=''>
            <img src={Pagment} width='100%' height='400px' alt='logo' />
          </div>
           
        <div className='title-2'>
          <hr/>
             <h2>FORMA DE PAGAMENTO</h2>
             <hr/>
        </div>

          <form className='Corpo-F'>
            <div className='Nome-C'>

              <h4>Nome do cartão: </h4>
              <input type='text' className='form-control' name='NomeCartao' placeholder='Nome completo, como mostrado no cartão' required value={this.state.Cnome} onChange={this.Cnome} />
            </div>


            <div className="Pagamento">
              <label> <h4>Pagamento: </h4> </label>
           &nbsp;<select value={this.state.Pagamento} onChange={this.Pagamento} name="Pagamento" required>
                <option value="">Selecione</option>
                <option value="Cartão de credito">Cartão de crédito</option>
                <option value="Cartão de debito">Cartão de débito</option>
                <option value="PayPal">PayPal</option>
              </select>
            </div>

            <div className='numero-C'>
              <h4>Número do cartão de crédito: </h4>
              <input type='text' className='form-control' name='numeroC' placeholder='' required value={this.state.NumeroCartao} onChange={this.NumeroC} />
            </div>

            <div className='data-E'>
              <h4>Data de expiração: </h4>
              <input type='text' className='form-control' name='Cartao' placeholder='' required value={this.state.Data} onChange={this.Data} />
            </div>

            <div className='cvv'>
              <h4>CVV: </h4>
              <input type='text' className='form-control' name='Cartao' placeholder='' required value={this.state.cvv} onChange={this.CVV} />
            </div>


            <div className="Final">
              <input type="submit" value="FINALIZAR COMPRA" onClick={() => this.chamaFuction()} />
            </div>
          </form>

        </div >
      )

    }

  };
}

export default form;
